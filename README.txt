
This module enables automatically importing directories as galleries of images. For each gallery, a photobar preview of the gallery is created for display on the main-page. Where this module shines is it's simplicity and automation. Photos need simply to be placed in the required directory on the server, and the photobars will automatically be created and inserted in the correct place in the site. Unlike other photo managing modules, no installation is required nor does it require altering Drupal's database.

It is best suited for use with cron to completely automate the process of creating galleries.  Cron will automatically find all photo directories and create a photobar and gallery for them.  If a gallery already exists by a given date, new photos will be added to the gallery.

See http://students.washington.edu/jkivligh/drupal_photobar/docs/index.php for the most up-to-date and complete documentation.

Author
-----
Jason Kivlighn <jkivlighn@gmail.com>

Usage
-----
Put folder in the specified directory ('files/photos' by default), run cron, and watch your photobar and gallery be created :-)  Photobars can manually be added by placing the folder of photos in the appropriate directory and specifying that folder in create content->photobar.

Installation
------------
Simply copy photobar.module into drupal's modules/ directory and enable it within drupal.  It will also require the 'image' and 'image_gallery' modules to be installed.

License
-------
Photobar is licensed under the GPL.
